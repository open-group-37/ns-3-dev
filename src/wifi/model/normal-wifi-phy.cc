
#include <algorithm>
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "wifi-utils.h"
#include "error-rate-model.h"
#include "normal-wifi-phy.h"
#include "ap-wifi-mac.h"
#include "wifi-radio-energy-model.h"
#include "frame-capture-model.h"
#include "preamble-detection-model.h"
#include "wifi-net-device.h"
#include "wifi-psdu.h"


namespace ns3 {

TypeId
NormalWifiPhy::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::NormalWifiPhy")
    .SetParent<WifiPhy> ()
    .SetGroupName ("Wifi")
    .AddAttribute ("RxNoiseFigure",
                   "Loss (dB) in the Signal-to-Noise-Ratio due to non-idealities in the receiver."
                   " According to Wikipedia (http://en.wikipedia.org/wiki/Noise_figure), this is "
                   "\"the difference in decibels (dB) between"
                   " the noise output of the actual receiver to the noise output of an "
                   " ideal receiver with the same overall gain and bandwidth when the receivers "
                   " are connected to sources at the standard noise temperature T0 (usually 290 K)\".",
                   DoubleValue (7),
                   MakeDoubleAccessor (&NormalWifiPhy::SetRxNoiseFigure),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}

NormalWifiPhy::NormalWifiPhy ()
  : m_currentEvent (0),    
    m_timeLastPreambleDetected (Seconds (0)),
    m_ofdmaStarted (false)
{
  NS_LOG_FUNCTION (this);  
}

NormalWifiPhy::~NormalWifiPhy ()
{
  NS_LOG_FUNCTION (this);
}

void
NormalWifiPhy::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_currentPreambleEvents.clear ();
  WifiPhy::DoDispose ();
}

void
NormalWifiPhy::DoInitialize (void)
{
  NS_LOG_FUNCTION (this);
  WifiPhy::DoInitialize ();
}

/****************************************************************
 *       Public part
 ****************************************************************/

void
NormalWifiPhy::Send (WifiPsduMap psdus, WifiTxVector txVector)
{
  NS_LOG_FUNCTION (this << psdus << txVector);
  /* Transmission can happen if:
   *  - we are syncing on a packet. It is the responsibility of the
   *    MAC layer to avoid doing this but the PHY does nothing to
   *    prevent it.
   *  - we are idle
   */
  NS_ASSERT (!m_state->IsStateTx () && !m_state->IsStateSwitching ());

  if (txVector.GetNssMax () > GetMaxSupportedTxSpatialStreams ())
    {
      NS_FATAL_ERROR ("Unsupported number of spatial streams!");
    }

  if (m_state->IsStateSleep ())
    {
      NS_LOG_DEBUG ("Dropping packet because in sleep mode");
      for (auto const& psdu : psdus)
        {
          NotifyTxDrop (psdu.second);
        }
      return;
    }
  
  Time txDuration;
  if (txVector.GetPreambleType () == WIFI_PREAMBLE_HE_TB)
    {
      NS_ASSERT (txVector.GetLength () > 0);
      txDuration = ConvertLSigLengthToHeTbPpduDuration (txVector.GetLength (), txVector, GetFrequency ());
    }
  else
    {
      txDuration = CalculateTxDuration (psdus, txVector, GetFrequency ());
    }

  if (!m_endPreambleDetectionEvents.empty () || m_currentEvent != 0)
    {
      MaybeCcaBusyDuration (); //needed to keep busy state afterwards
    }

  for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
    {
      endPreambleDetectionEvent.Cancel ();
    }
  m_endPreambleDetectionEvents.clear ();
  if (m_endPlcpRxEvent.IsRunning ())
    {
      m_endPlcpRxEvent.Cancel ();
    }
  for (auto & endRxEvent : m_endRxEvents)
    {
      endRxEvent.Cancel ();
    }
  m_endRxEvents.clear ();
  if (m_state->IsStateRx ())
    {
      m_interference.NotifyRxEnd (Simulator::Now ());
    }
  m_currentEvent = 0;
  m_currentPreambleEvents.clear ();

  if (m_powerRestricted)
    {
      NS_LOG_DEBUG ("Transmitting with power restriction");
    }
  else
    {
      NS_LOG_DEBUG ("Transmitting without power restriction");
    }

  NotifyTxBegin (psdus, DbmToW (GetTxPowerForTransmission (txVector) + GetTxGain ()));
  for (auto const& psdu : psdus)
    {
      NotifyMonitorSniffTx (psdu.second, GetFrequency (), txVector, psdu.first);
    }
  m_state->SwitchToTx (txDuration, psdus, GetPowerDbm (txVector.GetTxPowerLevel ()), txVector);

  if (m_state->GetState () == WifiPhyState::OFF)
    {
      NS_LOG_DEBUG ("Transmission canceled because device is OFF");
      return;
    }

  uint64_t uid;
  if (txVector.GetPreambleType () == WIFI_PREAMBLE_HE_TB)
    {
      //Use UID of PPDU containing trigger frame to identify resulting HE TB PPDUs, since the latter should immediately follow the former
      NS_ASSERT (m_previouslyRxPpduUid != UINT64_MAX);
      uid = m_previouslyRxPpduUid;
    }
  else
    {
      uid = m_globalPpduUid++;
    }
  m_previouslyRxPpduUid = UINT64_MAX; //reset to use it only once
  Ptr<WifiPpdu> ppdu = Create<WifiPpdu> (psdus, txVector, txDuration, GetFrequency (), uid);

  if (m_wifiRadioEnergyModel != 0 && m_wifiRadioEnergyModel->GetMaximumTimeInState (WifiPhyState::TX) < txDuration)
    {
      ppdu->SetTruncatedTx ();
    }

  StartTx (ppdu, txVector.GetTxPowerLevel ()); //now that the content of the TXVECTOR is stored in the WifiPpdu through PHY headers, the method calling StartTx has to specify the TX power level to use upon transmission

  m_channelAccessRequested = false;
  m_powerRestricted = false;

  Simulator::Schedule (txDuration, &NormalWifiPhy::Reset, this);
}

void
NormalWifiPhy::ResetCca (bool powerRestricted, double txPowerMaxSiso, double txPowerMaxMimo)
{
  NS_LOG_FUNCTION (this << powerRestricted << txPowerMaxSiso << txPowerMaxMimo);
  m_powerRestricted = powerRestricted;
  m_txPowerMaxSiso = txPowerMaxSiso;
  m_txPowerMaxMimo = txPowerMaxMimo;
  NS_ASSERT ((m_currentEvent->GetEndTime () - Simulator::Now ()).IsPositive ());
  Simulator::Schedule (m_currentEvent->GetEndTime () - Simulator::Now (), &WifiPhy::EndReceiveInterBss, this);
  AbortCurrentReception (OBSS_PD_CCA_RESET);
}

void
NormalWifiPhy::StartReceivePreamble (Ptr<WifiPpdu> ppdu, RxPowerWattPerChannelBand rxPowersW)
{
  //The total RX power corresponds to the maximum over all the bands
  auto it = std::max_element (rxPowersW.begin (), rxPowersW.end (),
    [] (const std::pair<WifiSpectrumBand, double>& p1, const std::pair<WifiSpectrumBand, double>& p2) {
      return p1.second < p2.second;
    });
  NS_LOG_FUNCTION (this << *ppdu << it->second);
  WifiTxVector txVector = ppdu->GetTxVector ();
  Time rxDuration = ppdu->GetTxDuration ();

  Ptr<Event> event;
  //We store all incoming preamble events, and a decision is made at the end of the preamble detection window.
  //If a preamble is received after the preamble detection window, it is stored anyway because this is needed for HE TB PPDUs in
  //order to properly update the received power in InterferenceHelper. The map is cleaned anyway at the end of the current reception.
  if (ppdu->IsUlMu ())
    {
      auto it = m_currentPreambleEvents.find (ppdu->GetUid ());
      if (it != m_currentPreambleEvents.end ())
        {
          NS_LOG_DEBUG ("Received another HE TB PPDU for UID " << ppdu->GetUid () << " from STA-ID " << ppdu->GetStaId () << " and BSS color " << +txVector.GetBssColor ());
          event = it->second;
          if (Simulator::Now () - event->GetStartTime () > NanoSeconds (400))
            {
              //Section 27.3.14.3 from 802.11ax Draft 4.0: Pre-correction accuracy requirements.
              //A STA that transmits an HE TB PPDU, non-HT PPDU, or non-HT duplicate PPDU in response to a triggering PPDU
              //shall ensure that the transmission start time of the HE TB PPDU, non-HT PPDU, or non-HT duplicate PPDU is
              //within ±0.4 µs + 16 µs from the end, at the STA’s antenna connector, of the last OFDM symbol of the triggering
              //PPDU (if it contains no PE field) or of the PE field of the triggering PPDU (if the PE field is present).
              //As a result, if an HE TB PPDU arrives later than 0.4 µs, it is added as an interference but PPDU is dropped.
              event = m_interference.Add (ppdu, txVector, rxDuration, rxPowersW);
              NS_LOG_DEBUG ("Drop packet because not received within the 400ns window");
              NotifyRxDrop (GetAddressedPsduInPpdu (ppdu), NOT_ALLOWED);
            }
          else
            {
              //Update received power of the event associated to that UL MU transmission
              m_interference.UpdateEvent (event, rxPowersW);
            }
          if ((m_state->GetState () == WifiPhyState::RX) && (m_currentEvent->GetPpdu ()->GetUid () != ppdu->GetUid ()))
            {
              NS_LOG_DEBUG ("Drop packet because already in Rx");
              NotifyRxDrop (GetAddressedPsduInPpdu (ppdu), NOT_ALLOWED);
            }
          return;
        }
      else
        {
          NS_LOG_DEBUG ("Received a new HE TB PPDU for UID " << ppdu->GetUid () << " from STA-ID " << ppdu->GetStaId () << " and BSS color " << +txVector.GetBssColor ());
          event = m_interference.Add (ppdu, txVector, CalculatePlcpPreambleAndHeaderDuration (txVector), rxPowersW); //the OFDMA part of the transmission will be added later on
          m_currentPreambleEvents.insert ({ppdu->GetUid (), event});
        }
    }
  else
    {
      event = m_interference.Add (ppdu, txVector, rxDuration, rxPowersW);
      NS_ASSERT (m_currentPreambleEvents.find (ppdu->GetUid ()) == m_currentPreambleEvents.end ());
      m_currentPreambleEvents.insert ({ppdu->GetUid (), event});
    }

  if (m_state->GetState () == WifiPhyState::OFF)
    {
      NS_LOG_DEBUG ("Cannot start RX because device is OFF");
      return;
    }

  if (ppdu->IsTruncatedTx ())
    {
      NS_LOG_DEBUG ("Packet reception stopped because transmitter has been switched off");
      return;
    }

  Time endRx = Simulator::Now () + rxDuration;
  switch (m_state->GetState ())
    {
    case WifiPhyState::SWITCHING:
      NS_LOG_DEBUG ("drop packet because of channel switching");
      NotifyRxDrop (GetAddressedPsduInPpdu (ppdu), NOT_ALLOWED);
      /*
       * Packets received on the upcoming channel are added to the event list
       * during the switching state. This way the medium can be correctly sensed
       * when the device listens to the channel for the first time after the
       * switching e.g. after channel switching, the channel may be sensed as
       * busy due to other devices' tramissions started before the end of
       * the switching.
       */
      if (endRx > Simulator::Now () + m_state->GetDelayUntilIdle ())
        {
          //that packet will be noise _after_ the completion of the channel switching.
          MaybeCcaBusyDuration ();
          return;
        }
      break;
    case WifiPhyState::RX:
      if (m_frameCaptureModel != 0
          && m_frameCaptureModel->CaptureNewFrame (m_currentEvent->GetRxPowerW (), event->GetRxPowerW (), m_currentEvent->GetStartTime ()))
        {
          AbortCurrentReception (FRAME_CAPTURE_PACKET_SWITCH);
          NS_LOG_DEBUG ("Switch to new packet");
          StartRx (event);
        }
      else
        {
          NS_LOG_DEBUG ("Drop packet because already in Rx");
          NotifyRxDrop (GetAddressedPsduInPpdu (ppdu), NOT_ALLOWED);
          if (m_currentEvent == 0)
            {
              /*
               * We are here because the non-legacy PHY header has not been successfully received.
               * The PHY is kept in RX state for the duration of the PPDU, but EndReceive function is
               * not called when the reception of the PPDU is finished, which is responsible to clear
               * m_currentPreambleEvents. As a result, m_currentPreambleEvents should be cleared here.
               */
              m_currentPreambleEvents.clear ();
            }
          if (endRx > Simulator::Now () + m_state->GetDelayUntilIdle ())
            {
              //that packet will be noise _after_ the reception of the currently-received packet.
              MaybeCcaBusyDuration ();
              return;
            }
        }
      break;
    case WifiPhyState::TX:
      NS_LOG_DEBUG ("Drop packet because already in Tx");
      NotifyRxDrop (GetAddressedPsduInPpdu (ppdu), NOT_ALLOWED);
      if (endRx > Simulator::Now () + m_state->GetDelayUntilIdle ())
        {
          //that packet will be noise _after_ the transmission of the currently-transmitted packet.
          MaybeCcaBusyDuration ();
          return;
        }
      break;
    case WifiPhyState::CCA_BUSY:
    case WifiPhyState::IDLE:
      StartRx (event);
      break;
    case WifiPhyState::SLEEP:
      NS_LOG_DEBUG ("Drop packet because in sleep mode");
      NotifyRxDrop (GetAddressedPsduInPpdu (ppdu), NOT_ALLOWED);
      break;
    default:
      NS_FATAL_ERROR ("Invalid WifiPhy state.");
      break;
    }
}

void
NormalWifiPhy::StartReceiveHeader (Ptr<Event> event)
{
  NS_LOG_FUNCTION (this << *event);
  NS_ASSERT (!IsStateRx ());
  NS_ASSERT (m_endPlcpRxEvent.IsExpired ());
  NS_ASSERT (m_endRxEvents.empty ());
  double maxRxPowerW = 0;
  Ptr<Event> maxEvent;
  NS_ASSERT (!m_currentPreambleEvents.empty());
  for (auto preambleEvent : m_currentPreambleEvents)
  {
    double rxPowerW = preambleEvent.second->GetRxPowerW ();
    if (rxPowerW > maxRxPowerW)
      {
        maxRxPowerW = rxPowerW;
        maxEvent = preambleEvent.second;
      }
  }

  if (maxEvent != event)
    {
      NS_LOG_DEBUG ("Receiver got a stronger packet with UID " << maxEvent->GetPpdu ()->GetUid () << " during preamble detection: drop packet with UID " << event->GetPpdu ()->GetUid ());
      NotifyRxDrop (GetAddressedPsduInPpdu (event->GetPpdu ()), PREAMBLE_DETECTION_PACKET_SWITCH);
      auto it = m_currentPreambleEvents.find (event->GetPpdu ()->GetUid ());
      m_currentPreambleEvents.erase (it);
      return;
    }

  m_currentEvent = event;

  //This is needed to cleanup the m_firstPowerPerBand so that the first power corresponds to the power at the start of the PPDU
  m_interference.NotifyRxEnd (m_currentEvent->GetStartTime ());
  //Make sure InterferenceHelper keeps recording events
  m_interference.NotifyRxStart ();

  uint16_t channelWidth;
  if (m_currentEvent->GetTxVector ().GetChannelWidth () >= 40)
    {
      channelWidth = 20; //calculate PER on the 20 MHz primary channel for PHY headers
    }
  else
    {
      channelWidth = m_currentEvent->GetTxVector ().GetChannelWidth ();
    }
  
  double snr = m_interference.CalculateSnr (m_currentEvent, channelWidth, GetBand (channelWidth));
  NS_LOG_DEBUG ("SNR(dB)=" << RatioToDb (snr) << " at start of legacy PHY header");

  Time headerPayloadDuration = m_currentEvent->GetStartTime () + m_currentEvent->GetPpdu ()->GetTxDuration () - Simulator::Now ();
  if (!m_preambleDetectionModel || (m_preambleDetectionModel->IsPreambleDetected (m_currentEvent->GetRxPowerW (), snr, GetChannelWidth ())))
    {
      for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
        {
          endPreambleDetectionEvent.Cancel ();
        }
      m_endPreambleDetectionEvents.clear ();

      for (auto it = m_currentPreambleEvents.begin (); it != m_currentPreambleEvents.end (); )
        {
          if (it->second != m_currentEvent)
            {
              NS_LOG_DEBUG ("Drop packet with UID " << it->first << " arrived at time " << it->second->GetStartTime ());
              NotifyRxDrop (GetAddressedPsduInPpdu (it->second->GetPpdu ()), PREAMBLE_DETECTION_PACKET_SWITCH);
              it = m_currentPreambleEvents.erase (it);
            }
          else
            {
              it++;
            }
        }
  
      m_state->SwitchToRx (headerPayloadDuration);
      NotifyRxBegin (GetAddressedPsduInPpdu (m_currentEvent->GetPpdu ()));

      m_timeLastPreambleDetected = Simulator::Now ();

      WifiTxVector txVector = m_currentEvent->GetTxVector ();

      uint8_t nss = txVector.GetNssMax();
      if (txVector.GetPreambleType () == WIFI_PREAMBLE_HE_MU)
        {
          uint16_t staId = GetStaId (m_currentEvent->GetPpdu ());
          for (auto info : txVector.GetHeMuUserInfoMap ())
            {
              if (info.first == staId)
                {
                  nss = info.second.nss; //no need to look at other PSDUs
                  break;
                }
            }
        }
      if (nss > GetMaxSupportedRxSpatialStreams ())
        {
          NS_LOG_DEBUG ("Packet reception could not be started because not enough RX antennas");
          NotifyRxDrop (GetAddressedPsduInPpdu (m_currentEvent->GetPpdu ()), UNSUPPORTED_SETTINGS);
          m_interference.NotifyRxEnd (Simulator::Now ());
          m_currentEvent = 0;
          m_currentPreambleEvents.clear ();
          MaybeCcaBusyDuration ();
          return;
        }

      if ((txVector.GetChannelWidth () >= 40) && (txVector.GetChannelWidth () > GetChannelWidth ()))
        {
          NS_LOG_DEBUG ("Packet reception could not be started because not enough channel width");
          NotifyRxDrop (GetAddressedPsduInPpdu (m_currentEvent->GetPpdu ()), UNSUPPORTED_SETTINGS);
          m_interference.NotifyRxEnd (Simulator::Now ());
          m_currentEvent = 0;
          m_currentPreambleEvents.clear ();
          MaybeCcaBusyDuration ();
          return;
        }

      if (txVector.GetPreambleType () == WIFI_PREAMBLE_HT_GF)
        {
          //No legacy PHY header for HT GF
          Time remainingPreambleHeaderDuration = CalculatePlcpPreambleAndHeaderDuration (txVector) - (Simulator::Now () - m_currentEvent->GetStartTime ());
          m_endPlcpRxEvent = Simulator::Schedule (remainingPreambleHeaderDuration, &NormalWifiPhy::StartReceivePayload, this, m_currentEvent);
        }
      else
        {
          //Schedule end of legacy PHY header
          Time remainingPreambleAndLegacyHeaderDuration = GetPlcpPreambleDuration (txVector) + GetPlcpHeaderDuration (txVector) - (Simulator::Now () - m_currentEvent->GetStartTime ());
          m_endPlcpRxEvent = Simulator::Schedule (remainingPreambleAndLegacyHeaderDuration, &NormalWifiPhy::ContinueReceiveHeader, this, m_currentEvent);
        }
    }
  else
    {
      NS_LOG_DEBUG ("Drop packet because PHY preamble detection failed");
      NotifyRxDrop (GetAddressedPsduInPpdu (m_currentEvent->GetPpdu ()), PREAMBLE_DETECT_FAILURE);
      for (auto it = m_currentPreambleEvents.begin (); it != m_currentPreambleEvents.end (); ++it)
        {
          if (it->second == m_currentEvent)
            {
              it = m_currentPreambleEvents.erase (it);
              break;
            }
        }
      if (m_currentPreambleEvents.empty())
        {
          //Do not erase events if there are still pending preamble events to be processed
          m_interference.NotifyRxEnd (Simulator::Now ());
        }
      m_currentEvent = 0;
    }
  // Like CCA-SD, CCA-ED is governed by the 4μs CCA window to flag CCA-BUSY
  // for any received signal greater than the CCA-ED threshold.
  MaybeCcaBusyDuration ();
}

void
NormalWifiPhy::ContinueReceiveHeader (Ptr<Event> event)
{
  NS_LOG_FUNCTION (this << *event);
  NS_ASSERT (IsStateRx ());
  NS_ASSERT (m_endPlcpRxEvent.IsExpired ());

  uint16_t channelWidth;
  if (event->GetTxVector ().GetChannelWidth () >= 40)
    {
      channelWidth = 20; //calculate PER on the 20 MHz primary channel for PHY headers
    }
  else
    {
      channelWidth = event->GetTxVector ().GetChannelWidth ();
    }
  InterferenceHelper::SnrPer snrPer = m_interference.CalculateLegacyPhyHeaderSnrPer (event, GetBand (channelWidth));

  NS_LOG_DEBUG ("SNR(dB)=" << RatioToDb (snrPer.snr) << ", PER=" << snrPer.per);
  if (m_random->GetValue () > snrPer.per) //legacy PHY header reception succeeded
    {
      NS_LOG_DEBUG ("Received legacy PHY header");
      WifiTxVector txVector = event->GetTxVector ();
      Time remainingPreambleHeaderDuration = CalculatePlcpPreambleAndHeaderDuration (txVector) - GetPlcpPreambleDuration (txVector) - GetPlcpHeaderDuration (txVector);
      m_endPlcpRxEvent = Simulator::Schedule (remainingPreambleHeaderDuration, &NormalWifiPhy::StartReceivePayload, this, event);
    }
  else //legacy PHY header reception failed
    {
      NS_LOG_DEBUG ("Abort reception because legacy PHY header reception failed");
      AbortCurrentReception (L_SIG_FAILURE);
      m_currentPreambleEvents.clear ();
    }
}

void
NormalWifiPhy::StartReceivePayload (Ptr<Event> event)
{
  NS_LOG_FUNCTION (this << *event);
  NS_ASSERT (m_endPlcpRxEvent.IsExpired ());
  bool canReceivePayload = false;
  Ptr<const WifiPpdu> ppdu = event->GetPpdu ();
  WifiModulationClass modulation = ppdu->GetModulation ();

  if (ppdu->GetTxVector ().GetPreambleType () == WIFI_PREAMBLE_HE_TB)
    {
      Ptr<WifiNetDevice> device = DynamicCast<WifiNetDevice> (GetDevice ());
      bool isAp = (DynamicCast<ApWifiMac> (device->GetMac ()) != 0);
      if (!isAp)
        {
          NS_LOG_INFO ("Ignore UL-OFDMA (OFDMA part of HE TB PPDU) received by STA");
          for (auto it = m_currentPreambleEvents.begin (); it != m_currentPreambleEvents.end (); ++it)
          {
            if (it->second == event)
              {
                it = m_currentPreambleEvents.erase (it);
                break;
              }
          }
          if (m_currentPreambleEvents.empty ())
            {
              Reset ();
            }
          m_interference.NotifyRxEnd (Simulator::Now ());
          return;
        }
    }
  
  if ((modulation == WIFI_MOD_CLASS_HT) || (modulation == WIFI_MOD_CLASS_VHT) || (modulation == WIFI_MOD_CLASS_HE))
    {
      uint16_t channelWidth;
      if (event->GetTxVector ().GetChannelWidth () >= 40)
        {
          channelWidth = 20; //calculate PER on the 20 MHz primary channel for PHY headers
        }
      else
        {
          channelWidth = event->GetTxVector ().GetChannelWidth ();
        }
      InterferenceHelper::SnrPer snrPer = m_interference.CalculateNonLegacyPhyHeaderSnrPer (event, GetBand (channelWidth));
      NS_LOG_DEBUG ("SNR(dB)=" << RatioToDb (snrPer.snr) << ", PER=" << snrPer.per);
      canReceivePayload = (m_random->GetValue () > snrPer.per);
    }
  else
    {
      //If we are here, this means legacy PHY header was already successfully received
      canReceivePayload = true;
    }
  if (canReceivePayload) //plcp reception succeeded
    {
      Ptr<const WifiPsdu> psdu = GetAddressedPsduInPpdu (ppdu);
      if (psdu)
        {
          uint16_t staId = GetStaId (ppdu);
          WifiTxVector txVector = event->GetTxVector ();
          WifiMode txMode = txVector.GetMode (staId);
          if (IsModeSupported (txMode) || IsMcsSupported (txMode))
            {
              NS_LOG_DEBUG ("Receiving PSDU");
              m_signalNoiseMap.insert ({std::make_pair (ppdu->GetUid (), staId), SignalNoiseDbm ()});
              m_statusPerMpduMap.insert ({std::make_pair (ppdu->GetUid (), staId), std::vector<bool> ()});
              if (psdu->GetNMpdus () > 1 && txVector.GetPreambleType () != WIFI_PREAMBLE_HE_TB)
                {
                  // for HE TB PPDUs, ScheduleEndOfMpdus is called by StartReceiveOfdmaPayload
                  ScheduleEndOfMpdus (event);
                }
              Time payloadDuration = ppdu->GetTxDuration () - CalculatePlcpPreambleAndHeaderDuration (txVector);
              m_phyRxPayloadBeginTrace (txVector, payloadDuration); //this callback (equivalent to PHY-RXSTART primitive) is triggered only if headers have been correctly decoded and that the mode within is supported
              if (txVector.GetPreambleType () == WIFI_PREAMBLE_HE_TB)
                {
                  m_currentHeTbPpduUid = ppdu->GetUid ();
                  //EndReceive is scheduled by StartReceiveOfdmaPayload
                }
              else
                {
                  m_endRxEvents.push_back (Simulator::Schedule (payloadDuration, &NormalWifiPhy::EndReceive, this, event));
                }
            }
          else //mode is not allowed
            {
              NS_LOG_DEBUG ("Drop packet because it was sent using an unsupported mode (" << txMode << ")");
              NotifyRxDrop (GetAddressedPsduInPpdu (event->GetPpdu ()), UNSUPPORTED_SETTINGS);
              m_interference.NotifyRxEnd (m_currentEvent->GetEndTime ());
              m_currentEvent = 0;
              m_currentPreambleEvents.clear ();
            }
        }
      else
        {
          NS_ASSERT (ppdu->IsMu ());
          NS_LOG_DEBUG ("No PSDU addressed to that PHY in the received MU PPDU");
          m_interference.NotifyRxEnd (m_currentEvent->GetEndTime ());
          m_currentEvent = 0;
          m_currentPreambleEvents.clear ();
        }
      if (modulation == WIFI_MOD_CLASS_HE)
        {
          HePreambleParameters params;
          params.rssiW = event->GetRxPowerW ();
          params.bssColor = event->GetTxVector ().GetBssColor ();
          NotifyEndOfHePreamble (params);
        }
    }
  else //plcp reception failed
    {
      NS_LOG_DEBUG ("Drop packet because non-legacy PHY header reception failed");
      NotifyRxDrop (GetAddressedPsduInPpdu (event->GetPpdu ()), SIG_A_FAILURE);
      m_interference.NotifyRxEnd (m_currentEvent->GetEndTime ());
      m_currentEvent = 0;
      m_currentPreambleEvents.clear ();
    }
}

void
NormalWifiPhy::StartReceiveOfdmaPayload (Ptr<WifiPpdu> ppdu, RxPowerWattPerChannelBand rxPowersW)
{
  //The total RX power corresponds to the maximum over all the bands
  auto it = std::max_element (rxPowersW.begin (), rxPowersW.end (),
    [] (const std::pair<WifiSpectrumBand, double>& p1, const std::pair<WifiSpectrumBand, double>& p2) {
      return p1.second < p2.second;
    });
  NS_LOG_FUNCTION (this << *ppdu << it->second);
  WifiTxVector txVector = ppdu->GetTxVector ();
  Time payloadDuration = ppdu->GetTxDuration () - CalculatePlcpPreambleAndHeaderDuration (txVector);
  Ptr<Event> event = m_interference.Add (ppdu, txVector, payloadDuration, rxPowersW, !m_ofdmaStarted);
  Ptr<const WifiPsdu> psdu = GetAddressedPsduInPpdu (ppdu);
  if (psdu->GetNMpdus () > 1)
    {
      ScheduleEndOfMpdus (event);
    }
  m_endRxEvents.push_back (Simulator::Schedule (payloadDuration, &NormalWifiPhy::EndReceive, this, event));
  m_ofdmaStarted = true;
  m_signalNoiseMap.insert ({std::make_pair (ppdu->GetUid (), ppdu->GetStaId ()), SignalNoiseDbm ()});
  m_statusPerMpduMap.insert ({std::make_pair (ppdu->GetUid (), ppdu->GetStaId ()), std::vector<bool> ()});
}

void
NormalWifiPhy::SetRxNoiseFigure (double noiseFigureDb)
{
  NS_LOG_FUNCTION (this << noiseFigureDb);
  m_interference.SetNoiseFigure (DbToRatio (noiseFigureDb));
  m_interference.SetNumberOfReceiveAntennas (GetNumberOfAntennas ());
}

void
NormalWifiPhy::SetErrorRateModel (const Ptr<ErrorRateModel> rate)
{
  m_errorRateModel = rate;
  m_interference.SetErrorRateModel (rate);
  m_interference.SetNumberOfReceiveAntennas (GetNumberOfAntennas ());
}

/****************************************************************
 *       Protected part
 ****************************************************************/

void
NormalWifiPhy::Reset (void)
{
  NS_LOG_FUNCTION (this);
  m_currentPreambleEvents.clear ();
  m_currentEvent = 0;
  m_ofdmaStarted = false;
}

bool
NormalWifiPhy::DoChannelSwitch (uint8_t nch)
{
  m_powerRestricted = false;
  m_channelAccessRequested = false;
  m_currentEvent = 0;
  m_currentPreambleEvents.clear ();
  if (!IsInitialized ())
    {
      //this is not channel switch, this is initialization
      NS_LOG_DEBUG ("initialize to channel " << +nch);
      return true;
    }

  NS_ASSERT (!IsStateSwitching ());
  switch (m_state->GetState ())
    {
    case WifiPhyState::RX:
      NS_LOG_DEBUG ("drop packet because of channel switching while reception");
      m_endPlcpRxEvent.Cancel ();
      for (auto & endRxEvent : m_endRxEvents)
        {
          endRxEvent.Cancel ();
        }
      m_endRxEvents.clear ();
      for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
        {
          endPreambleDetectionEvent.Cancel ();
        }
      m_endPreambleDetectionEvents.clear ();
      goto switchChannel;
      break;
    case WifiPhyState::TX:
      NS_LOG_DEBUG ("channel switching postponed until end of current transmission");
      Simulator::Schedule (GetDelayUntilIdle (), &WifiPhy::SetChannelNumber, this, nch);
      break;
    case WifiPhyState::CCA_BUSY:
    case WifiPhyState::IDLE:
        {
          for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
            {
              endPreambleDetectionEvent.Cancel ();
            }
          m_endPreambleDetectionEvents.clear ();
          for (auto & endRxEvent : m_endRxEvents)
            {
              endRxEvent.Cancel ();
            }
          m_endRxEvents.clear ();
        }
      goto switchChannel;
      break;
    case WifiPhyState::SLEEP:
      NS_LOG_DEBUG ("channel switching ignored in sleep mode");
      break;
    default:
      NS_ASSERT (false);
      break;
    }

  return false;

switchChannel:

  NS_LOG_DEBUG ("switching channel " << +GetChannelNumber () << " -> " << +nch);
  m_state->SwitchToChannelSwitching (GetChannelSwitchDelay ());
  m_interference.EraseEvents ();
  /*
   * Needed here to be able to correctly sensed the medium for the first
   * time after the switching. The actual switching is not performed until
   * after m_channelSwitchDelay. Packets received during the switching
   * state are added to the event list and are employed later to figure
   * out the state of the medium after the switching.
   */
  return true;
}

bool
NormalWifiPhy::DoFrequencySwitch (uint16_t frequency)
{
  m_powerRestricted = false;
  m_channelAccessRequested = false;
  m_currentEvent = 0;
  m_currentPreambleEvents.clear ();
  if (!IsInitialized ())
    {
      //this is not channel switch, this is initialization
      NS_LOG_DEBUG ("start at frequency " << frequency);
      return true;
    }

  NS_ASSERT (!IsStateSwitching ());
  switch (m_state->GetState ())
    {
    case WifiPhyState::RX:
      NS_LOG_DEBUG ("drop packet because of channel/frequency switching while reception");
      m_endPlcpRxEvent.Cancel ();
      for (auto & endRxEvent : m_endRxEvents)
        {
          endRxEvent.Cancel ();
        }
      m_endRxEvents.clear ();
      for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
        {
          endPreambleDetectionEvent.Cancel ();
        }
      m_endPreambleDetectionEvents.clear ();
      goto switchFrequency;
      break;
    case WifiPhyState::TX:
      NS_LOG_DEBUG ("channel/frequency switching postponed until end of current transmission");
      Simulator::Schedule (GetDelayUntilIdle (), &WifiPhy::SetFrequency, this, frequency);
      break;
    case WifiPhyState::CCA_BUSY:
    case WifiPhyState::IDLE:
      for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
        {
          endPreambleDetectionEvent.Cancel ();
        }
      m_endPreambleDetectionEvents.clear ();
      for (auto & endRxEvent : m_endRxEvents)
        {
          endRxEvent.Cancel ();
        }
      m_endRxEvents.clear ();
      goto switchFrequency;
      break;
    case WifiPhyState::SLEEP:
      NS_LOG_DEBUG ("frequency switching ignored in sleep mode");
      break;
    default:
      NS_ASSERT (false);
      break;
    }

  return false;

switchFrequency:

  NS_LOG_DEBUG ("switching frequency " << GetFrequency () << " -> " << frequency);
  m_state->SwitchToChannelSwitching (GetChannelSwitchDelay ());
  m_interference.EraseEvents ();
  /*
   * Needed here to be able to correctly sensed the medium for the first
   * time after the switching. The actual switching is not performed until
   * after m_channelSwitchDelay. Packets received during the switching
   * state are added to the event list and are employed later to figure
   * out the state of the medium after the switching.
   */
  return true;
}

void
NormalWifiPhy::SwitchMaybeToCcaBusy (void)
{
  NS_LOG_FUNCTION (this);
  //We are here because we have received the first bit of a packet and we are
  //not going to be able to synchronize on it
  //In this model, CCA becomes busy when the aggregation of all signals as
  //tracked by the InterferenceHelper class is higher than the CcaBusyThreshold

  uint16_t primaryChannelWidth = GetChannelWidth () >= 40 ? 20 : GetChannelWidth ();
  Time delayUntilCcaEnd = m_interference.GetEnergyDuration (m_ccaEdThresholdW, GetBand (primaryChannelWidth));
  if (!delayUntilCcaEnd.IsZero ())
    {
      NS_LOG_DEBUG ("Calling SwitchMaybeToCcaBusy for " << delayUntilCcaEnd.As (Time::S));
      m_state->SwitchMaybeToCcaBusy (delayUntilCcaEnd);
    }
}

WifiSpectrumBand
NormalWifiPhy::GetBand (uint16_t /*bandWidth*/, uint8_t /*bandIndex*/)
{
  WifiSpectrumBand band;
  band.first = 0;
  band.second = 0;
  return band;
}

WifiSpectrumBand
NormalWifiPhy::ConvertHeRuSubcarriers (uint16_t channelWidth, HeRu::SubcarrierRange range) const
{
  NS_ASSERT_MSG (false, "802.11ax can only be used with SpectrumWifiPhy");
  WifiSpectrumBand convertedSubcarriers;
  return convertedSubcarriers;
}

WifiSpectrumBand
NormalWifiPhy::GetRuBand (WifiTxVector txVector, uint16_t staId)
{
  NS_ASSERT (txVector.IsMu ());
  WifiSpectrumBand band;
  HeRu::RuSpec ru = txVector.GetRu (staId);
  uint16_t channelWidth = txVector.GetChannelWidth ();
  NS_ASSERT (channelWidth <= GetChannelWidth ());
  HeRu::SubcarrierGroup group = HeRu::GetSubcarrierGroup (channelWidth, ru.ruType, ru.index);
  HeRu::SubcarrierRange range = std::make_pair (group.front ().first, group.back ().second);
  band = ConvertHeRuSubcarriers (channelWidth, range);
  return band;
}

/****************************************************************
 *       Private part
 ****************************************************************/


void
NormalWifiPhy::StartRx (Ptr<Event> event)
{
  double rxPowerW = event->GetRxPowerW ();
  NS_LOG_FUNCTION (this << *event << rxPowerW);

  NS_LOG_DEBUG ("sync to signal (power=" << rxPowerW << "W)");
  m_interference.NotifyRxStart (); //We need to notify it now so that it starts recording events

  m_endPreambleDetectionEvents.push_back (Simulator::Schedule (GetPreambleDetectionDuration (), &NormalWifiPhy::StartReceiveHeader, this, event));
}

std::pair<bool, SignalNoiseDbm>
NormalWifiPhy::GetReceptionStatus (Ptr<const WifiPsdu> psdu, Ptr<Event> event, uint16_t staId,
                             Time relativeMpduStart, Time mpduDuration)
{
  NS_LOG_FUNCTION (this << *psdu << *event << staId << relativeMpduStart << mpduDuration);
  uint16_t channelWidth = std::min (GetChannelWidth (), event->GetTxVector ().GetChannelWidth ());
  WifiTxVector txVector = event->GetTxVector ();
  WifiSpectrumBand band;
  if (txVector.IsMu ())
    {
      band = GetRuBand (txVector, staId);
      channelWidth = HeRu::GetBandwidth (txVector.GetRu (staId).ruType);
    }
  else
    {
      band = GetBand (channelWidth);
    }
  InterferenceHelper::SnrPer snrPer = m_interference.CalculatePayloadSnrPer (event, channelWidth, band, staId, std::make_pair (relativeMpduStart, relativeMpduStart + mpduDuration));

  WifiMode mode = event->GetTxVector ().GetMode (staId);
  NS_LOG_DEBUG ("rate=" << (mode.GetDataRate (event->GetTxVector (), staId)) <<
                ", SNR(dB)=" << RatioToDb (snrPer.snr) << ", PER=" << snrPer.per << ", size=" << psdu->GetSize () <<
                ", relativeStart = " << relativeMpduStart.GetNanoSeconds () << "ns, duration = " << mpduDuration.GetNanoSeconds () << "ns");

  // There are two error checks: PER and receive error model check.
  // PER check models is typical for Wi-Fi and is based on signal modulation;
  // Receive error model is optional, if we have an error model and
  // it indicates that the packet is corrupt, drop the packet.
  SignalNoiseDbm signalNoise;
  signalNoise.signal = WToDbm (event->GetRxPowerW ());
  signalNoise.noise = WToDbm (event->GetRxPowerW () / snrPer.snr);
  if (m_random->GetValue () > snrPer.per &&
      !(m_postReceptionErrorModel && m_postReceptionErrorModel->IsCorrupt (psdu->GetPacket ()->Copy ())))
    {
      NS_LOG_DEBUG ("Reception succeeded: " << psdu);
      NotifyRxEnd (psdu);
      return std::make_pair (true, signalNoise);
    }
  else
    {
      NS_LOG_DEBUG ("Reception failed: " << psdu);
      NotifyRxDrop (psdu, ERRONEOUS_FRAME);
      return std::make_pair (false, signalNoise);
    }
}

void
NormalWifiPhy::EndReceive (Ptr<Event> event)
{
  Ptr<const WifiPpdu> ppdu = event->GetPpdu ();
  WifiTxVector txVector = event->GetTxVector ();
  Time psduDuration = ppdu->GetTxDuration () - CalculatePlcpPreambleAndHeaderDuration (txVector);
  NS_LOG_FUNCTION (this << *event << psduDuration);
  NS_ASSERT (event->GetEndTime () == Simulator::Now ());
  uint16_t staId = GetStaId (ppdu);
  uint16_t channelWidth = std::min (GetChannelWidth (), event->GetTxVector ().GetChannelWidth ());
  WifiSpectrumBand band;
  if (txVector.IsMu ())
    {
      band = GetRuBand (txVector, staId);
      channelWidth = HeRu::GetBandwidth (txVector.GetRu (staId).ruType);
    }
  else
    {
      band = GetBand (channelWidth);
    }
  double snr = m_interference.CalculateSnr (event, channelWidth, band);

  Ptr<const WifiPsdu> psdu = GetAddressedPsduInPpdu (ppdu);
  auto signalNoiseIt = m_signalNoiseMap.find (std::make_pair (ppdu->GetUid (), staId));
  NS_ASSERT (signalNoiseIt != m_signalNoiseMap.end ());
  auto statusPerMpduIt = m_statusPerMpduMap.find (std::make_pair (ppdu->GetUid (), staId));
  NS_ASSERT (statusPerMpduIt != m_statusPerMpduMap.end ());
  if (psdu->GetNMpdus () == 1)
    {
      //We do not enter here for A-MPDU since this is done in EndOfMpdu
      std::pair<bool, SignalNoiseDbm> rxInfo = GetReceptionStatus (psdu, event, staId, NanoSeconds (0), psduDuration);
      signalNoiseIt->second = rxInfo.second;
      statusPerMpduIt->second.push_back (rxInfo.first);
    }

if (std::count(statusPerMpduIt->second.begin (), statusPerMpduIt->second.end (), true))
   {
      //At least one MPDU has been successfully received
      NotifyMonitorSniffRx (psdu, GetFrequency (), txVector, signalNoiseIt->second, statusPerMpduIt->second, staId);
      RxSignalInfo rxSignalInfo;
      rxSignalInfo.snr = snr;
      rxSignalInfo.rssi = signalNoiseIt->second.signal; //same information for all MPDUs
      m_state->SwitchFromRxEndOk (Copy (psdu), rxSignalInfo, txVector, staId, statusPerMpduIt->second);
      m_previouslyRxPpduUid = event->GetPpdu ()->GetUid (); //store UID only if reception is successful (b/c otherwise trigger won't be read by MAC layer)
    }
  else
    {
      m_state->SwitchFromRxEndError (Copy (psdu), snr);
    }

  if (ppdu->IsUlMu ())
    {
      for (auto it = m_endRxEvents.begin (); it != m_endRxEvents.end (); )
        {
          if (it->IsExpired ())
            {
              it = m_endRxEvents.erase (it);
            }
          else
            {
              it++;
            }
        }
      if (m_endRxEvents.empty ())
        {
          //We got the last PPDU of the UL-OFDMA transmission
          m_interference.NotifyRxEnd (Simulator::Now ());
          m_signalNoiseMap.clear ();
          m_statusPerMpduMap.clear ();
          for (const auto & endOfMpduEvent : m_endOfMpduEvents)
            {
              NS_ASSERT (endOfMpduEvent.IsExpired ());
            }
          m_endOfMpduEvents.clear ();
          Reset ();
        }
    }
  else
    {
      m_interference.NotifyRxEnd (Simulator::Now ());
      m_currentEvent = 0;
      m_currentPreambleEvents.clear ();
      m_endRxEvents.clear ();
      m_signalNoiseMap.clear ();
      m_statusPerMpduMap.clear ();
      for (const auto & endOfMpduEvent : m_endOfMpduEvents)
        {
          NS_ASSERT (endOfMpduEvent.IsExpired ());
        }
      m_endOfMpduEvents.clear ();
    }
  MaybeCcaBusyDuration ();
}

void
NormalWifiPhy::EndOfMpdu (Ptr<Event> event, Ptr<const WifiPsdu> psdu, size_t mpduIndex, Time relativeStart, Time mpduDuration)
{
  NS_LOG_FUNCTION (this << *event << mpduIndex << relativeStart << mpduDuration);
  Ptr<const WifiPpdu> ppdu = event->GetPpdu ();
  WifiTxVector txVector = event->GetTxVector ();
  uint16_t staId = GetStaId (ppdu);  
  uint16_t channelWidth = std::min (GetChannelWidth (), event->GetTxVector ().GetChannelWidth ());
  WifiSpectrumBand band;
  if (txVector.IsMu ())
    {
      band = GetRuBand (txVector, staId);
      channelWidth = HeRu::GetBandwidth (txVector.GetRu (staId).ruType);
    }
  else
    {
      band = GetBand (channelWidth);
    }
  double snr = m_interference.CalculateSnr (event, channelWidth, band);

  std::pair<bool, SignalNoiseDbm> rxInfo = GetReceptionStatus (psdu, event, staId, relativeStart, mpduDuration);
  NS_LOG_DEBUG ("Extracted MPDU #" << mpduIndex << ": duration: " << mpduDuration.GetNanoSeconds () << "ns" <<
                ", correct reception: " << rxInfo.first << ", Signal/Noise: " << rxInfo.second.signal << "/" << rxInfo.second.noise << "dBm");

  auto signalNoiseIt = m_signalNoiseMap.find (std::make_pair (ppdu->GetUid (), staId));
  NS_ASSERT (signalNoiseIt != m_signalNoiseMap.end ());
  signalNoiseIt->second = rxInfo.second;

  RxSignalInfo rxSignalInfo;
  rxSignalInfo.snr = snr;
  rxSignalInfo.rssi = rxInfo.second.signal;

  auto statusPerMpduIt = m_statusPerMpduMap.find (std::make_pair (ppdu->GetUid (), staId));
  NS_ASSERT (statusPerMpduIt != m_statusPerMpduMap.end ());
  statusPerMpduIt->second.push_back (rxInfo.first);

  if (rxInfo.first)
    {
      m_state->ContinueRxNextMpdu (Copy (psdu), rxSignalInfo, txVector);
    }
}

void
NormalWifiPhy::ScheduleEndOfMpdus (Ptr<Event> event)
{
  NS_LOG_FUNCTION (this << *event);
  Ptr<const WifiPpdu> ppdu = event->GetPpdu ();
  Ptr<const WifiPsdu> psdu = GetAddressedPsduInPpdu (ppdu);
  WifiTxVector txVector = event->GetTxVector ();
  uint16_t staId = GetStaId (ppdu);
  Time endOfMpduDuration = NanoSeconds (0);
  Time relativeStart = NanoSeconds (0);
  Time psduDuration = ppdu->GetTxDuration () - CalculatePlcpPreambleAndHeaderDuration (txVector);
  Time remainingAmpduDuration = psduDuration;
  MpduType mpdutype = FIRST_MPDU_IN_AGGREGATE;
  uint32_t totalAmpduSize = 0;
  double totalAmpduNumSymbols = 0.0;
  size_t nMpdus = psdu->GetNMpdus ();
  auto mpdu = psdu->begin ();
  for (size_t i = 0; i < nMpdus && mpdu != psdu->end (); ++mpdu)
    {
      Time mpduDuration = GetPayloadDuration (psdu->GetAmpduSubframeSize (i), txVector,
                                              GetFrequency (), mpdutype, true, totalAmpduSize,
                                              totalAmpduNumSymbols, staId);

      remainingAmpduDuration -= mpduDuration;
      if (i == (nMpdus - 1) && !remainingAmpduDuration.IsZero ()) //no more MPDU coming
        {
          mpduDuration += remainingAmpduDuration; //apply a correction just in case rounding had induced slight shift
        }

      endOfMpduDuration += mpduDuration;
      m_endOfMpduEvents.push_back (Simulator::Schedule (endOfMpduDuration, &NormalWifiPhy::EndOfMpdu, this, event, Create<WifiPsdu> (*mpdu, false), i, relativeStart, mpduDuration));

      //Prepare next iteration
      ++i;
      relativeStart += mpduDuration;
      mpdutype = (i == (nMpdus - 1)) ? LAST_MPDU_IN_AGGREGATE : MIDDLE_MPDU_IN_AGGREGATE;
    }
}

void
NormalWifiPhy::AbortCurrentReception (WifiPhyRxfailureReason reason)
{
  NS_LOG_FUNCTION (this << reason);
  for (auto & endPreambleDetectionEvent : m_endPreambleDetectionEvents)
    {
      endPreambleDetectionEvent.Cancel ();
    }
  m_endPreambleDetectionEvents.clear ();
  if (m_endPlcpRxEvent.IsRunning ())
    {
      m_endPlcpRxEvent.Cancel ();
    }
  for (auto & endRxEvent : m_endRxEvents)
    {
      endRxEvent.Cancel ();
    }
  m_endRxEvents.clear ();
  NotifyRxDrop (GetAddressedPsduInPpdu (m_currentEvent->GetPpdu ()), reason);
  m_interference.NotifyRxEnd (Simulator::Now ());
  bool is_failure = (reason != OBSS_PD_CCA_RESET);
  m_state->SwitchFromRxAbort (is_failure);
  for (auto it = m_currentPreambleEvents.begin (); it != m_currentPreambleEvents.end (); ++it)
    {
      if (it->second == m_currentEvent)
        {
          it = m_currentPreambleEvents.erase (it);
          break;
        }
    }
  m_currentEvent = 0;
}

Time
NormalWifiPhy::DelayUntilCcaEnd () 
{
  uint16_t primaryChannelWidth = GetChannelWidth () >= 40 ? 20 : GetChannelWidth ();
  return m_interference.GetEnergyDuration (m_ccaEdThresholdW, GetBand (primaryChannelWidth));
}

} //namespace ns3