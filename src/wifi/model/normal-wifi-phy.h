#ifndef NORMAL_WIFI_PHY_H
#define NORMAL_WIFI_PHY_H

#include "wifi-phy.h"
#include "interference-helper.h"

namespace ns3 {

class NormalWifiPhy : public WifiPhy
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);

  NormalWifiPhy ();
  virtual ~NormalWifiPhy ();

  /**
   * \param psdus the PSDUs to send
   * \param txVector the TXVECTOR that has tx parameters such as mode, the transmission mode to use to send
   *        this PSDU, and txPowerLevel, a power level to use to send the whole PPDU. The real transmission
   *        power is calculated as txPowerMin + txPowerLevel * (txPowerMax - txPowerMin) / nTxLevels
   */
  void Send (WifiPsduMap psdus, WifiTxVector txVector);

  /**
   * \param ppdu the PPDU to send
   * \param txPowerLevel the power level to use
   *
   * Note that now that the content of the TXVECTOR is stored in the WifiPpdu through PHY headers,
   * the calling method has to specify the TX power level to use upon transmission.
   * Indeed the TXVECTOR obtained from WifiPpdu does not have this information set.
   */
  virtual void StartTx (Ptr<WifiPpdu> ppdu, uint8_t txPowerLevel) = 0;

  /**
   * Reset PHY to IDLE, with some potential TX power restrictions for the next transmission.
   *
   * \param powerRestricted flag whether the transmit power is restricted for the next transmission
   * \param txPowerMaxSiso the SISO transmit power retriction for the next transmission
   * \param txPowerMaxMimo the MIMO transmit power retriction for the next transmission
   */
  void ResetCca (bool powerRestricted, double txPowerMaxSiso = 0, double txPowerMaxMimo = 0);

  /**
   * Start receiving the PHY preamble of a PPDU (i.e. the first bit of the preamble has arrived).
   *
   * \param ppdu the arriving PPDU
   * \param rxPowersW the receive power in W per band
   */
  void StartReceivePreamble (Ptr<WifiPpdu> ppdu, RxPowerWattPerChannelBand rxPowersW);

  /**
   * Start receiving the PHY header of a PPDU (i.e. after the end of receiving the preamble).
   *
   * \param event the event holding incoming PPDU's information
   */
  void StartReceiveHeader (Ptr<Event> event);

  /**
   * Continue receiving the PHY header of a PPDU (i.e. after the end of receiving the legacy header part).
   *
   * \param event the event holding incoming PPDU's information
   */
  void ContinueReceiveHeader (Ptr<Event> event);

  /**
   * Start receiving the PSDU (i.e. the first symbol of the PSDU has arrived).
   *
   * \param event the event holding incoming PPDU's information
   */
  void StartReceivePayload (Ptr<Event> event);

  /**
   * Start receiving the PSDU (i.e. the first symbol of the PSDU has arrived) of an UL-OFDMA transmission.
   * This function is called upon the RX event corresponding to the OFDMA part of the UL MU PPDU.
   *
   * \param ppdu the arriving PPDU
   * \param rxPowersW the receive power in W per band
   */
  void StartReceiveOfdmaPayload (Ptr<WifiPpdu> ppdu, RxPowerWattPerChannelBand rxPowersW);

  /**
   * Sets the RX loss (dB) in the Signal-to-Noise-Ratio due to non-idealities in the receiver.
   *
   * \param noiseFigureDb noise figure in dB
   */
  void SetRxNoiseFigure (double noiseFigureDb);

  /**
   * Sets the error rate model.
   *
   * \param rate the error rate model
   */
  void SetErrorRateModel (const Ptr<ErrorRateModel> rate);

protected:
  // Inherited
  virtual void DoInitialize (void);
  virtual void DoDispose (void);

  /*
   * Reset data upon end of TX or RX
   */
  void Reset (void);

  /**
   * The default implementation does nothing and returns true.  This method
   * is typically called internally by SetChannelNumber ().
   *
   * \brief Perform any actions necessary when user changes channel number
   * \param id channel number to try to switch to
   * \return true if WifiPhy can actually change the number; false if not
   * \see SetChannelNumber
   */
  bool DoChannelSwitch (uint8_t id);
  /**
   * The default implementation does nothing and returns true.  This method
   * is typically called internally by SetFrequency ().
   *
   * \brief Perform any actions necessary when user changes frequency
   * \param frequency frequency to try to switch to
   * \return true if WifiPhy can actually change the frequency; false if not
   * \see SetFrequency
   */
  bool DoFrequencySwitch (uint16_t frequency);

  /**
   * Check if Phy state should move to CCA busy state based on current
   * state of interference tracker.  In this model, CCA becomes busy when
   * the aggregation of all signals as tracked by the InterferenceHelper
   * class is higher than the CcaEdThreshold
   */
  void SwitchMaybeToCcaBusy (void);

  /**
   * Get the start band index and the stop band index for a given band
   *
   * \param bandWidth the width of the band to be returned (MHz)
   * \param bandIndex the index of the band to be returned
   *
   * \return a pair of start and stop indexes that defines the band
   */
  virtual WifiSpectrumBand GetBand (uint16_t bandWidth, uint8_t bandIndex = 0);

  /**
   * \param channelWidth the total channel width (MHz) used for the OFDMA transmission
   * \param range the subcarrier range of the HE RU
   * \return the converted subcarriers
   *
   * This is a helper function to convert HE RU subcarriers, which are relative to the center frequency subcarrier, to the indexes used by the Spectrum model.
   */
  virtual WifiSpectrumBand ConvertHeRuSubcarriers (uint16_t channelWidth, HeRu::SubcarrierRange range) const;

  /**
   * Get the RU band used to transmit a PSDU to a given STA in a HE MU PPDU
   *
   * \param txVector the TXVECTOR used for the transmission
   * \param staId the STA-ID of the recipient
   *
   * \return the RU band used to transmit a PSDU to a given STA in a HE MU PPDU
   */
  WifiSpectrumBand GetRuBand (WifiTxVector txVector, uint16_t staId);

  Ptr<Event> m_currentEvent; //!< Hold the current event
  std::map <uint64_t /* UID*/, Ptr<Event> > m_currentPreambleEvents; //!< store event associated to a PPDU (that has a unique ID) whose preamble is being received
  
  InterferenceHelper m_interference;   //!< Pointer to InterferenceHelper

private:
  /**
   * Starting receiving the PPDU after having detected the medium is idle or after a reception switch.
   *
   * \param event the event holding incoming PPDU's information
   */
  void StartRx (Ptr<Event> event);
  /**
   * Get the reception status for the provided MPDU and notify.
   *
   * \param psdu the arriving MPDU formatted as a PSDU
   * \param event the event holding incoming PPDU's information
   * \param staId the station ID of the PSDU (only used for MU)
   * \param relativeMpduStart the relative start time of the MPDU within the A-MPDU. 0 for normal MPDUs
   * \param mpduDuration the duration of the MPDU
   *
   * \return information on MPDU reception: status, signal power (dBm), and noise power (in dBm)
   */
  std::pair<bool, SignalNoiseDbm> GetReceptionStatus (Ptr<const WifiPsdu> psdu,
                                                      Ptr<Event> event, uint16_t staId,
                                                      Time relativeMpduStart,
                                                      Time mpduDuration);
  /**
   * The last symbol of the PPDU has arrived.
   *
   * \param event the event holding incoming PPDU's information
   */
  void EndReceive (Ptr<Event> event);
  
  /**
   * The last symbol of an MPDU in an A-MPDU has arrived.
   *
   * \param event the event holding incoming PPDU's information
   * \param psdu the arriving MPDU formatted as a PSDU containing a normal MPDU
   * \param mpduIndex the index of the MPDU within the A-MPDU
   * \param relativeMpduStart the relative start time of the MPDU within the A-MPDU.
   * \param mpduDuration the duration of the MPDU
   */  
  void EndOfMpdu (Ptr<Event> event, Ptr<const WifiPsdu> psdu, size_t mpduIndex, Time relativeStart, Time mpduDuration);

  /**
   * Schedule end of MPDUs events.
   *
   * \param event the event holding incoming PPDU's information
   */
  void ScheduleEndOfMpdus (Ptr<Event> event);

  /**
   * Due to newly arrived signal, the current reception cannot be continued and has to be aborted
   * \param reason the reason the reception is aborted
   *
   */
  void AbortCurrentReception (WifiPhyRxfailureReason reason);

  Time DelayUntilCcaEnd ();

  Time m_timeLastPreambleDetected; //!< Record the time the last preamble was detected
  bool m_ofdmaStarted; //!< Flag whether the reception of the OFDMA part has started (only used for UL-OFDMA)
  /**
   * A pair of a UID and STA_ID
   */
  typedef std::pair <uint64_t /* uid */, uint16_t /* staId */> UidStaIdPair;

  std::map<UidStaIdPair, std::vector<bool> > m_statusPerMpduMap; //!< Map of the current reception status per MPDU that is filled in as long as MPDUs are being processed by the PHY in case of an A-MPDU
  std::map<UidStaIdPair, SignalNoiseDbm> m_signalNoiseMap; //!< Map of the latest signal power and noise power in dBm (noise power includes the noise figure)
};

}//namespace ns3

#endif /* NORMAL_WIFI_PHY_H */